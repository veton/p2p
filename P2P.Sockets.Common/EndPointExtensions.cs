using System.Net;

namespace P2P.Sockets.Common
{
    /// <summary>
    /// Extension methods for EndPoint class.
    /// </summary>
    public static class EndPointExtensions
    {
        /// <summary>
        /// Formats EndPoint.
        /// </summary>
        /// <param name="endPoint">EndPoint to format.</param>
        /// <returns>Formatted endpoint.</returns>
        public static string Format(this EndPoint endPoint)
        {
            var ipEndPoint = endPoint as IPEndPoint;
            return ipEndPoint != null
                ? string.Format("{0}:{1}", ipEndPoint.Address, ipEndPoint.Port)
                : endPoint.ToString();
        }
    }
}