using System;
using System.IO;
using System.Net.Sockets;
using System.Threading.Tasks;

namespace P2P.Sockets.Common
{
    /// <summary>
    /// Helper methods for working with Sockets.
    /// </summary>
    public static class SocketsHelper
    {
        /// <summary>
        /// Links output of one socket to input of another and vice versa.
        /// </summary>
        /// <param name="socket1">Socket 1.</param>
        /// <param name="socket2">Socket 2.</param>
        public static void LinkSockets(Socket socket1, Socket socket2)
        {
            Console.WriteLine(
                "Linking sockets: {0} <=> {1} <=> {2} <=> {3}",
                socket1.RemoteEndPoint.Format(),
                socket1.LocalEndPoint.Format(),
                socket2.LocalEndPoint.Format(),
                socket2.RemoteEndPoint.Format());

            var stream1 = new NetworkStream(socket1);
            var stream2 = new NetworkStream(socket2);
            Task.WaitAny(
                stream1.CopyToAsync(stream2),
                stream2.CopyToAsync(stream1));

            socket1.Close();
            socket2.Close();
            Console.WriteLine("Sockets disconnected");
        }
    }
}