﻿using System;
using System.Diagnostics;
using System.ServiceModel;
using System.Threading.Tasks;
using Microsoft.ServiceBus;
using P2P.Common;

namespace P2P.Azure.Client
{
    public class Program
    {
        private const string ClientPolicy = "Client";
        private const string ClientKey = @"";

        public static void Main(string[] args)
        {
            ConsoleHelper.Header("P2P Client");

            while (true)
            {
                Console.WriteLine();
                ConsoleHelper.Sleep("Press any key to send request ...");
                SendRequest();
            }
        }

        private static void SendRequest()
        {
            var binding = new BasicHttpRelayBinding();
            var serviceUri = ServiceBusEnvironment.CreateServiceUri("https", "zerto-test", "TestService");
            var endpoint = new EndpointAddress(serviceUri);
            var tokenProvider = TokenProvider.CreateSharedAccessSignatureTokenProvider(ClientPolicy, ClientKey);
            var endpointBehavior = new TransportClientEndpointBehavior {TokenProvider = tokenProvider};

            Console.WriteLine("Connectin to service in Azure Service Bus: {0}...", serviceUri);
            var channelFactory = new ChannelFactory<ITestService>(binding, endpoint);
            channelFactory.Endpoint.Behaviors.Add(endpointBehavior);
            ITestService client = channelFactory.CreateChannel();
            string request = string.Format("Request [PID={0}, TS={1:HH:mm:ss.fff}]", Process.GetCurrentProcess().Id, DateTime.Now);

            Console.WriteLine("Sending request:\t" + request);
            Task.Run(() =>
            {
                var response = client.TestMethod(request);
                Console.WriteLine("Response received:\t" + response);
            });
        }
    }
}
