﻿using System.ServiceModel;

namespace P2P.Common
{
    [ServiceContract(CallbackContract = typeof(ITestService))]
    public interface ITestServiceSession
    {
        [OperationContract]
        void Open();
    }
}