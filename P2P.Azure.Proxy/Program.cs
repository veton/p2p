﻿using System;
using System.ServiceModel;
using System.Threading;

using Microsoft.ServiceBus;

using P2P.Common;

namespace P2P.Azure.Proxy
{
    public class Program
    {
        private const string ServicePolicy = "Service";
        private const string ServiceKey = @"";

        public static void Main(string[] args)
        {
            ConsoleHelper.Header("P2P Azure Proxy");

            int port = ConsoleHelper.Prompt("Port", 9080);
            var endpoint = new EndpointAddress(string.Format("http://localhost:{0}/TestService", port));

            var client = CreateClient(endpoint);
            var proxy = new TestServiceProxy {Service = client};
            RegisterService(proxy);

            Thread.Sleep(Timeout.Infinite);
        }

        private static ITestService CreateClient(EndpointAddress endpoint)
        {
            Console.WriteLine("Connecting to service: " + endpoint);
            var binding = new BasicHttpBinding();
            var channelFactory = new ChannelFactory<ITestService>(binding, endpoint);
            ITestService client = channelFactory.CreateChannel();
            Console.WriteLine("Client created");
            return client;
        }

        private static void RegisterService(ITestService service)
        {
            var host = new ServiceHost(service);

            var serviceUri = ServiceBusEnvironment.CreateServiceUri("https", "zerto-test", "TestService");
            var binding = new BasicHttpRelayBinding();
            var serviceEndpoint = host.AddServiceEndpoint(typeof(ITestService), binding, serviceUri);

            var tokenProvider = TokenProvider.CreateSharedAccessSignatureTokenProvider(ServicePolicy, ServiceKey);
            var endpointBehavior = new TransportClientEndpointBehavior {TokenProvider = tokenProvider};
            serviceEndpoint.Behaviors.Add(endpointBehavior);

            Console.WriteLine("Registering service in Azure Service Bus: {0}...", serviceUri);
            host.Open();
            Console.WriteLine("Service registered");
        }
    }
}
