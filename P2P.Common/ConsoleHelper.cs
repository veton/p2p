﻿using System;
using System.Diagnostics;

namespace P2P.Common
{
    public static class ConsoleHelper
    {
        /// <summary>
        /// Prints header.
        /// </summary>
        /// <param name="appName">Header appName.</param>
        public static void Header(string appName)
        {
            int pid = Process.GetCurrentProcess().Id;
            Console.Title = string.Format("{0} (PID {1})", appName, pid);
            Console.WriteLine("*** {0} (PID {1}) ***", appName, pid);
        }

        /// <summary>
        /// Prompts user to enter value of specified parameter.
        /// </summary>
        /// <param name="name">Parameter name.</param>
        /// <param name="defaultValue">Default value.</param>
        /// <returns>User-entered value, or default value if input is empty.</returns>
        public static string Prompt(string name, string defaultValue)
        {
            Console.Write("Enter {0} ({1}): ", name, defaultValue);
            var value = Console.ReadLine();
            return !string.IsNullOrEmpty(value)
                ? value
                : defaultValue;
        }

        /// <summary>
        /// Prompts user to enter value of specified parameter.
        /// </summary>
        /// <param name="name">Parameter name.</param>
        /// <param name="defaultValue">Default value.</param>
        /// <returns>User-entered value, or default value if input is empty.</returns>
        public static int Prompt(string name, int defaultValue)
        {
            while (true)
            {
                string stringValue = Prompt(name, defaultValue.ToString());
                int value;
                if (int.TryParse(stringValue, out value))
                {
                    return value;
                }
            }
        }

        /// <summary>
        /// Waits for user to press any key.
        /// </summary>
        public static void Sleep(string message = "Press any key to continue ...")
        {
            ConsoleColor temp = Console.ForegroundColor;
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine();
            Console.WriteLine(message);
            Console.ForegroundColor = temp;

            Console.ReadKey(true);
        }
    }
}
