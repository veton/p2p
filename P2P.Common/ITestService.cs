﻿using System.ServiceModel;

namespace P2P.Common
{
    [ServiceContract]
    public interface ITestService
    {
        [OperationContract]
        string TestMethod(string request);
    }
}