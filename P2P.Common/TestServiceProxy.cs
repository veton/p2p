﻿using System;
using System.ServiceModel;

namespace P2P.Common
{
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.Single)]
    public class TestServiceProxy : ITestService
    {
        public ITestService Service { get; set; }

        public string TestMethod(string request)
        {
            if (Service == null)
            {
                throw new InvalidOperationException("Service is not initialized");
            }

            Console.WriteLine("Redirecting request: " + request);
            var response = Service.TestMethod(request);
            Console.WriteLine("Redirecting response: " + response);

            return response;
        }
    }
}