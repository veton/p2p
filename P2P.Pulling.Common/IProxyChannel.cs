﻿using System.ServiceModel;

namespace P2P.Polling.Common
{
    [ServiceContract]
    public interface IProxyChannel
    {
        [OperationContract]
        string GetRequest();

        [OperationContract]
        void SetResponse(string response);
    }
}
