﻿using System;
using System.Net.Sockets;
using System.Threading.Tasks;

using P2P.Common;
using P2P.Sockets.Common;

namespace P2P.Sockets.ProxyClient
{
    public class Program
    {
        public static void Main(string[] args)
        {
            ConsoleHelper.Header("P2P Sockets Proxy Client");

            int proxyPort = ConsoleHelper.Prompt("Proxy Communication Port", 9090);
            int servicePort = ConsoleHelper.Prompt("Proxy Service Port", 9080);

            while (true)
            {
                Console.WriteLine();

                var proxySocketTask = Task.Run(() => ConnectSocket(proxyPort));
                var serviceSocketTask = Task.Run(() => ConnectSocket(servicePort));
                Task.WaitAll(
                    proxySocketTask,
                    serviceSocketTask);

                SocketsHelper.LinkSockets(proxySocketTask.Result, serviceSocketTask.Result);
            }
        }

        private static Socket ConnectSocket(int port)
        {
            Console.WriteLine("Connecting socket to localhost:{0}...", port);
            var socket = new Socket(SocketType.Stream, ProtocolType.IP);
            socket.Connect("localhost", port);
            Console.WriteLine(
                "Socket connected: {0} => {1}",
                socket.LocalEndPoint.Format(),
                socket.RemoteEndPoint.Format());

            return socket;
        }
    }
}
