﻿using System;
using System.ServiceModel;
using System.Threading;
using P2P.Common;

namespace P2P.Server
{
    public class Program
    {
        public static void Main(string[] args)
        {
           ConsoleHelper.Header("P2P Server");

            int port = ConsoleHelper.Prompt("Port", 9080);
            var baseAddress = new Uri(string.Format("http://localhost:{0}/TestService", port));

            using (var host = new ServiceHost(typeof(TestService), baseAddress))
            {
                Console.WriteLine("Starting Service: " + baseAddress);
                host.Open();
                Console.WriteLine("Service started");

                Thread.Sleep(Timeout.Infinite);
            }
        }
    }
}
