﻿using System;
using System.Diagnostics;
using P2P.Common;

namespace P2P.Server
{
    public class TestService : ITestService
    {
        public string TestMethod(string request)
        {
            Console.WriteLine("Request:\t" + request);
            return string.Format("Response [PID={0}, TS={1:HH:mm:ss.fff}]", Process.GetCurrentProcess().Id, DateTime.Now);
        }
    }
}