﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Threading.Tasks;

using P2P.Common;
using P2P.Sockets.Common;

namespace P2P.Sockets.ProxyServer
{
    public class Program
    {
        public static void Main(string[] args)
        {
            ConsoleHelper.Header("P2P Sockets Proxy Server");

            int servicePort = ConsoleHelper.Prompt("Proxy Service Port", 9081);
            int proxyPort = ConsoleHelper.Prompt("Proxy Communication Port", 9090);

            Socket serviceSocket = ListenSocket(servicePort);
            Socket proxySocket = ListenSocket(proxyPort);

            while (true)
            {
                Console.WriteLine();

                var serviceSocketTask = Task.Run(() => AcceptSocket(serviceSocket));
                var proxySocketTask = Task.Run(() => AcceptSocket(proxySocket));
                Task.WaitAll(
                    serviceSocketTask,
                    proxySocketTask);

                SocketsHelper.LinkSockets(serviceSocketTask.Result, proxySocketTask.Result);
            }
        }

        private static Socket AcceptSocket(Socket socket)
        {
            var acceptedSocket = socket.Accept();
            Console.WriteLine(
                "Socket accepted: {0} => {1} ",
                acceptedSocket.RemoteEndPoint.Format(),
                acceptedSocket.LocalEndPoint.Format());

            return acceptedSocket;
        }

        private static Socket ListenSocket(int port)
        {
            var socket = new Socket(SocketType.Stream, ProtocolType.IP);
            var endPoint = new IPEndPoint(IPAddress.Any, port);
            socket.Bind(endPoint);
            socket.Listen(10);
            Console.WriteLine("Listening socket on {0}...", endPoint.Format());
            return socket;
        }
    }
}
