﻿using System;
using System.ServiceModel;
using System.Threading;
using P2P.Common;
using P2P.Polling.Common;
using Timer = System.Timers.Timer;

namespace P2P.Polling.ProxyClient
{
    public class Program
    {
        private const int PollingInterval = 5000;

        public static void Main(string[] args)
        {
            ConsoleHelper.Header("P2P Polling Proxy Client");

            int servicePort = ConsoleHelper.Prompt("Server Port", 9080);
            int proxyPort = ConsoleHelper.Prompt("Proxy Channel Port", 9090);

            var serviceEndpoint = new EndpointAddress(string.Format("http://{0}:{1}/TestService", "localhost", servicePort));
            var proxyEndpoint = new EndpointAddress(string.Format("http://{0}:{1}/TestProxy", "localhost", proxyPort));

            var proxyClient = CreateProxyClient(proxyEndpoint);
            var serviceClient = CreateServiceClient(serviceEndpoint);
            StartPolling(serviceClient, proxyClient);

            Thread.Sleep(Timeout.Infinite);
        }

        private static ITestService CreateServiceClient(EndpointAddress endpoint)
        {
            var binding = new BasicHttpBinding();
            var channelFactory = new ChannelFactory<ITestService>(binding, endpoint);

            Console.WriteLine("Connecting to Service: " + endpoint);
            var serviceClient = channelFactory.CreateChannel();
            Console.WriteLine("Service connected");

            return serviceClient;
        }

        private static IProxyChannel CreateProxyClient(EndpointAddress endpoint)
        {
            var binding = new BasicHttpBinding();
            var channelFactory = new ChannelFactory<IProxyChannel>(binding, endpoint);

            Console.WriteLine("Connecting to Proxy Channel: " + endpoint);
            var proxyClient = channelFactory.CreateChannel();
            Console.WriteLine("Proxy Communication connected");

            return proxyClient;
        }

        private static void StartPolling(ITestService serviceClient, IProxyChannel proxyClient)
        {
            Console.WriteLine("Polling every {0:N0}ms ... ", PollingInterval);
            var timer = new Timer(PollingInterval);
            timer.Elapsed += (s, ea) => ProcessRequests(serviceClient, proxyClient);
            timer.Start();
        }

        private static void ProcessRequests(ITestService serviceClient, IProxyChannel proxyClient)
        {
            Console.WriteLine("Polling proxy server ... ");
            while (true)
            {
                var request = proxyClient.GetRequest();
                if (request == null)
                {
                    return;
                }

                Console.WriteLine("Request obtained:\t" + request);
                var response = serviceClient.TestMethod(request);
                proxyClient.SetResponse(response);
                Console.WriteLine("Response sent:\t" + response);
            }
        }
    }
}
