﻿using System;
using System.ServiceModel;
using P2P.Common;

namespace P2P.WCF.ProxyServer
{
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.Single)] 
    public class TestServiceSession : ITestServiceSession
    {
        public ITestService Service { get; private set; }

        public event EventHandler Opened;

        public void Open()
        {
            Service = OperationContext.Current.GetCallbackChannel<ITestService>();
            OnOpened();
        }

        protected virtual void OnOpened()
        {
            var handler = Opened;
            if (handler != null)
            {
                handler(this, EventArgs.Empty);
            }
        }
    }
}