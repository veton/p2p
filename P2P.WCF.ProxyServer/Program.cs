﻿using System;
using System.ServiceModel;

using P2P.Common;

namespace P2P.WCF.ProxyServer
{
    public class Program
    {
        public static void Main(string[] args)
        {
            ConsoleHelper.Header("P2P WCF Proxy Server");

            int proxyPort = ConsoleHelper.Prompt("Proxy Channel Port", 9090);
            var proxyAdddress = new Uri(string.Format("http://localhost:{0}/TestProxy", proxyPort));

            int servicePort = ConsoleHelper.Prompt("Proxy Service Port", 9081);
            var serviceAddress = new Uri(string.Format("http://localhost:{0}/TestService", servicePort));

            var session = new TestServiceSession();
            CreateProxyEndpoint(session, proxyAdddress);
            CreteServiceEndpoint(session, serviceAddress);

            ConsoleHelper.Sleep();
        }

        private static void CreateProxyEndpoint(TestServiceSession session, Uri proxyAdddress)
        {
            var binding = new WSDualHttpBinding();
            var proxyHost = new ServiceHost(session);
            proxyHost.AddServiceEndpoint(typeof(ITestServiceSession), binding, proxyAdddress);

            Console.WriteLine("Starting Proxy Communication: " + proxyAdddress);
            proxyHost.Open();
            Console.WriteLine("Proxy Communication started");
        }

        private static void CreteServiceEndpoint(TestServiceSession session, Uri serviceAddress)
        {
            var proxy = new TestServiceProxy();
            session.Opened += (s, ea) => proxy.Service = session.Service;

            var serviceHost = new ServiceHost(proxy, serviceAddress);

            Console.WriteLine("Starting Proxy Service: " + serviceAddress);
            serviceHost.Open();
            Console.WriteLine("Proxy Service started");
        }
    }
}
