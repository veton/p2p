﻿using System;
using System.Diagnostics;
using System.ServiceModel;
using System.Threading.Tasks;

using P2P.Common;

namespace P2P.Client
{
    public class Program
    {
        private static readonly BasicHttpBinding Binding = new BasicHttpBinding();

        public static void Main(string[] args)
        {
            ConsoleHelper.Header("P2P Client");

            string address = "localhost"; // ConsoleHelper.Prompt("Server Address", "localhost");
            int port = ConsoleHelper.Prompt("Server Port", 9081);
            var endpoint = new EndpointAddress(string.Format("http://{0}:{1}/TestService", address, port));

            while (true)
            {
                Console.WriteLine();
                ConsoleHelper.Sleep("Press any key to send request ...");
                SendRequest(endpoint);
            }
        }

        private static void SendRequest(EndpointAddress endpoint)
        {
            var channelFactory = new ChannelFactory<ITestService>(Binding, endpoint);
            ITestService client = channelFactory.CreateChannel();
            string request = string.Format("Request [PID={0}, TS={1:HH:mm:ss.fff}]", Process.GetCurrentProcess().Id, DateTime.Now);

            Console.WriteLine("Sending request:\t" + request);
            Task.Run(() =>
            {
                var response = client.TestMethod(request);
                Console.WriteLine("Response received:\t" + response);
            });
        }
    }
}
