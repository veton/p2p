﻿using System;
using System.ServiceModel;

using P2P.Common;

namespace P2P.WCF.ProxyClient
{
    public class Program
    {
        public static void Main(string[] args)
        {
            ConsoleHelper.Header("P2P WCF Proxy Client");

            string serviceAddress = "localhost"; // ConsoleHelper.Prompt("Server Address", "localhost");
            int servicePort = ConsoleHelper.Prompt("Server Port", 9080);
            var serviceEndpoint = new EndpointAddress(string.Format("http://{0}:{1}/TestService", serviceAddress, servicePort));

            string proxyAddress = "localhost"; // ConsoleHelper.Prompt("Proxy Server Address", "localhost");
            int proxyPort = ConsoleHelper.Prompt("Proxy Communication Port", 9090);
            var proxyEndpoint = new EndpointAddress(string.Format("http://{0}:{1}/TestProxy", proxyAddress, proxyPort));

            var serviceClient = CreateServiceClient(serviceEndpoint);
            CreateProxyClient(serviceClient, proxyEndpoint);

            ConsoleHelper.Sleep();
        }

        private static ITestService CreateServiceClient(EndpointAddress endpoint)
        {
            var binding = new BasicHttpBinding();
            var channelFactory = new ChannelFactory<ITestService>(binding, endpoint);

            Console.WriteLine("Connecting to Service: " + endpoint);
            var serviceClient = channelFactory.CreateChannel();
            Console.WriteLine("Service connected");

            return serviceClient;
        }

        private static void CreateProxyClient(ITestService client, EndpointAddress endpoint)
        {
            var proxy = new TestServiceProxy {Service = client};
            var binding = new WSDualHttpBinding();
            var channelFactory = new DuplexChannelFactory<ITestServiceSession>(proxy, binding, endpoint);

            Console.WriteLine("Connecting to Proxy Channel: " + endpoint);
            var proxyClient = channelFactory.CreateChannel();
            Console.WriteLine("Proxy Channel connected");
            proxyClient.Open();
            Console.WriteLine("Proxy Channel session opened");
        }
    }
}
