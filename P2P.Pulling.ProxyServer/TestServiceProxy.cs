﻿using System;
using System.ServiceModel;

using P2P.Common;

namespace P2P.Polling.ProxyServer
{
    [ServiceBehavior(
        ConcurrencyMode = ConcurrencyMode.Multiple,
        InstanceContextMode = InstanceContextMode.Single)]
    public class TestServiceProxy : ITestService
    {
        private readonly TaskQueue _queue;

        public TestServiceProxy(TaskQueue queue)
        {
            _queue = queue;
        }

        public string TestMethod(string request)
        {
            _queue.Requests.Enqueue(request);
            Console.WriteLine("Reqest queued. Queue size: " + _queue.Requests.Count);
            return _queue.Responses.Take();
        }
    }
}
