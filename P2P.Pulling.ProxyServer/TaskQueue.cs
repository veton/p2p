using System.Collections.Concurrent;
using System.Collections.Generic;

namespace P2P.Polling.ProxyServer
{
    public class TaskQueue
    {
        public TaskQueue()
        {
            Requests = new Queue<string>();
            Responses = new BlockingCollection<string>();
        }

        public Queue<string> Requests { get; private set; }
        public BlockingCollection<string> Responses { get; private set; }
    }
}
