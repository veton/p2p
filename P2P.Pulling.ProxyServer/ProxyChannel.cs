﻿using System;
using System.ServiceModel;
using P2P.Polling.Common;

namespace P2P.Polling.ProxyServer
{
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.Single)]
    public class ProxyChannel : IProxyChannel
    {
        private readonly TaskQueue _queue;

        public ProxyChannel(TaskQueue queue)
        {
            _queue = queue;
        }

        public string GetRequest()
        {
            return _queue.Requests.Count > 0
                ? _queue.Requests.Dequeue()
                : null;
        }

        public void SetResponse(string response)
        {
            _queue.Responses.Add(response);
            Console.WriteLine("Response queued. Queue size: " + _queue.Responses.Count);
        }
    }
}
