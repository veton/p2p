﻿using System;
using System.ServiceModel;
using System.Threading;
using P2P.Common;

namespace P2P.Polling.ProxyServer
{
    public class Program
    {
        public static void Main(string[] args)
        {
            ConsoleHelper.Header("P2P Polling Proxy Server");

            int proxyPort = ConsoleHelper.Prompt("Proxy Channel Port", 9090);
            var proxyAdddress = new Uri(string.Format("http://localhost:{0}/TestProxy", proxyPort));

            int servicePort = ConsoleHelper.Prompt("Proxy Service Port", 9081);
            var serviceAddress = new Uri(string.Format("http://localhost:{0}/TestService", servicePort));

            var queue = new TaskQueue();
            CreteServiceEndpoint(queue, serviceAddress);
            CreateProxyEndpoint(queue, proxyAdddress);

            Thread.Sleep(Timeout.Infinite);
        }

        private static void CreateProxyEndpoint(TaskQueue queue, Uri proxyAdddress)
        {
            var service = new ProxyChannel(queue);
            var proxyHost = new ServiceHost(service, proxyAdddress);

            Console.WriteLine("Starting Proxy Channel: " + proxyAdddress);
            proxyHost.Open();
            Console.WriteLine("Proxy Channel started");
        }

        private static void CreteServiceEndpoint(TaskQueue queue, Uri serviceAddress)
        {
            var service = new TestServiceProxy(queue);
            var serviceHost = new ServiceHost(service, serviceAddress);

            Console.WriteLine("Starting Proxy Service: " + serviceAddress);
            serviceHost.Open();
            Console.WriteLine("Proxy Service started");
        }
    }
}
